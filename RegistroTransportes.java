
//import java.util.Iterator;
import java.util.Set;
import java.util.HashMap;


/**
 * @author Facundo Taramasco
 * @version 23/7/2014
 */
public class RegistroTransportes {

    private static HashMap<String, Transporte> transportes = new HashMap<String, Transporte>();

    /**
     * Metodo que verifica si existe un transporte en el HashMap mediante su patente.
     * @return true si existe, false caso contrario.
     */
    public static boolean verificarTransporte(String pat) {
        if ( transportes.containsKey(pat) ) {
            return true;
        } else {
            return false;
        }
    }


    public static boolean agregarTransporte(Transporte t) {
        if ( !verificarTransporte(t.getPatente()) ) {
            transportes.put(t.getPatente(), t);
            return true;
        } else {
            return false;
        }
    }

    public static void listaTransportes(){
        /*
        Iterator<Transporte> i = transportes.keySet().iterator();
        while ( i.hasNext() ){
            System.out.println( i.next() );
        }*/
        Set <String> claves = transportes.keySet();
        
        for (String clave : claves){
            System.out.println( transportes.get(clave) );
        }
    }

    public static String desplegarFactura(String patente){
        if ( verificarTransporte(patente) ) {
            return transportes.get(patente).desplegarFactura(); // polimorfismo en estado puro
        } else {
            return "\nTransporte Inexistente.";
        }
    }

    public static boolean agregarKmsRecorridos(String patente, int k){
        if ( verificarTransporte(patente) ){
            transportes.get(patente).agregarKilometrosRecorridos(k);
            return true;
        } else {
            return false;
        }
    }

    public static boolean reiniciarKmsRecorridos(String patente){
        if ( verificarTransporte(patente) ){
            transportes.get(patente).reiniciarKms();
            return true;
        } else {
            return false;
        }
    }

    public static Transporte devolverTransporte(String pat){
        return transportes.get(pat);
    }
}
