
/**
 * @author Facundo Taramasco
 * @version 22/7/2014
 */
public class TransporteLibreUso extends Transporte {

    private final int VALOR_KM_ADICIONAL = 700;

    // constructors
    public TransporteLibreUso() {
        super("", "", 100, 500);
    }

    public TransporteLibreUso(String p, String c, int ka, int cm) {
        super(p, c, ka, cm);
    }

    // abstracts implementation
    public float calcularGastoMes() {
        return getCostoMantencion() + (getKilometrosRecorridos() * VALOR_KM) + VALOR_KM_ADICIONAL;
    }

    public String desplegarFactura() {
        return "Desplegando Factura...\n"+
            "* Patente               : "+getPatente()+"\n"+
            "* Tipo de Transporte    : Transporte Libre Uso \n"+
            "* Kilometros Asignados  : "+getKilometrosAsignados()+" Kms.\n"+
            "* Kilometros Recorridos : "+getKilometrosRecorridos()+" Kms.\n"+
            "* Monto a pagar         : $"+calcularGastoMes() ;
    }

    // customs
        public String toString() {
        return super.toString()+
            "\nTipo Transporte  : Transporte Libre uso\n"+
             "Km. adicional    : "+VALOR_KM_ADICIONAL+
            "\nGasto del mes    : "+calcularGastoMes()+"\n";
    }

}
