// hardoceado
package paquetesPropios;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;

/**
 * Clase de proposito general que se encarga de la entrada por teclado en la terminal.
 * @author Facundo Taramasco
 * @version 23/7/2014
 */
public class Consola{
 

    public static void main(String[] args){
        
    }

    public static void MenuApp(String m){
        Consola.limpiarConsola();
        System.out.println(m);
    }

    public static String ingresoString(String msj){
        String ingreso;
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        System.out.printf("%s ",msj);
        try{
            ingreso = bf.readLine();
        }catch(IOException ioe){
            ingreso = "";
        }
        return ingreso;
    }

    public static int ingresoInt(String msj){
        boolean ok = false;
        int value = 0;
        do{
            try{
                value = new Integer( ingresoString(msj) );
                ok = true;
            }catch(NumberFormatException nfe){
                ok = false;
                //System.out.println("No es entero!");
            }
        }while ( !ok );
        return value;
    }

    public static double ingresoDouble(String msj){
        boolean ok = false;
        double value = 0.0;
        do{
            try{
                value = new Double( ingresoString(msj) );
                ok = true;
            }catch(NumberFormatException nfe){
                ok = false;
                //System.out.println("No es double!");
            }
        }while ( !ok );
        return value;
    }

    public static String ingresoSexo(){
        boolean ok = false;
        int opt = -1;
        String sexo = "";
        do {
            System.out.println("Ingrese sexo : \n" + 
                               "1. Hombre (H) \n" + 
                               "2. Mujer  (M)");
            opt = Consola.ingresoInt("Opcion : ");
            switch( opt ) {
                case 1:
                    sexo = "Hombre";
                    ok = true;
                    break;
                case 2:
                    sexo = "Mujer";
                    ok = true;
                    break;
            }
        } while ( !ok );
        return sexo;
    }

    public static void limpiarConsola(){
        for (int i = 0 ; i < 100 ; i++){
            System.out.println("");
        }
    }

    public static void msjContinuar(){
        System.out.printf("Presione Enter para continuar ...");
        try{
           System.in.read();
        }catch(IOException ioe){
        }
    }

}