/**
 * Abstract class Transporte - write a description of the class here
 * 
 * @author: Facundo Taramasco
 * Date: 21/7/2014
 */
public abstract class Transporte implements ICalculo {

    private String patente;
    private String conductor;
    private int kilometrosAsignados;
    private int kilometrosRecorridos;
    private int costoMantencion;
    private boolean enFuncionamiento;

    // constructors
    public Transporte() {
        this("", "", 500, 100);
    }

    public Transporte(String p, String c, int ka, int cm) {
        setPatente(p);
        setConductor(c); 
        setKilometrosAsignados(ka); 
        setKilometrosRecorridos(0);
        setCostoMantencion(cm);
        setEnFuncionamiento(true);
    }

    // getters
    public String getPatente() { return patente; }
    public String getConductor() { return conductor; }
    public int getKilometrosAsignados() { return kilometrosAsignados; }
    public int getKilometrosRecorridos() { return kilometrosRecorridos; }
    public int getCostoMantencion() { return costoMantencion; }
    public boolean getEnFuncionamiento() { return enFuncionamiento; }

    // setters
    public void setPatente(String p) { patente = p; }
    public void setConductor(String c) { conductor = c; }
    public void setKilometrosAsignados(int k) {
        if (k > 0)
            kilometrosAsignados = k;
    }
    public void setKilometrosRecorridos(int k) {
        if (k >= 0)
            kilometrosRecorridos = k;
    }
    public void setCostoMantencion(int c) { costoMantencion = c; }
    public void setEnFuncionamiento(boolean e) { enFuncionamiento = e; }

    // customs abstracts
    public abstract float calcularGastoMes();
    public abstract String desplegarFactura();
    
    // customs
    public void reiniciarKms(){
        System.out.println("Reiniciando kms de Transporte...");
        //setKilometrosAsignados(0);
        setKilometrosRecorridos(0);
    }

    public void agregarKilometrosRecorridos(int k){
        setKilometrosRecorridos( k + getKilometrosRecorridos() );
    }

    public String toString(){
        return "Patente          : " + getPatente() + "\n" +
               "Conductor        : " + getConductor() + "\n" +
               "Kms. Asignados   : " + getKilometrosAsignados() + "\n" +
               "Kms. Recorridos  : " + getKilometrosRecorridos() + "\n" +
               "Costo Mantencion : " + getCostoMantencion() + "\n" +
               "Funcionamiento   : " + getEnFuncionamiento();
    }
}
