
/**
 * Write a description of class TransporteLimitado here.
 * 
 * @author Facundo Taramasco
 * @version 21/7/2014
 */
public class TransporteLimitado extends Transporte {

    // constructors
    public TransporteLimitado() {
        super("", "", 100, 500);
    }

    public TransporteLimitado(String p, String c, int ka, int cm) {
        super(p, c, ka, cm);
    }

    // abstracts implementation
    public String desplegarFactura() {
        return "Desplegando Factura...\n"+
            "* Patente               : "+getPatente()+"\n"+
            "* Tipo de Transporte    : Transporte Limitado \n"+
            "* Kilometros Asignados  : "+getKilometrosAsignados()+" Kms.\n"+
            "* Kilometros Recorridos : "+getKilometrosRecorridos()+" Kms.\n"+
            "* Monto a pagar         : $"+calcularGastoMes() ;
    }

    public float calcularGastoMes() {
        return getCostoMantencion()+(getKilometrosRecorridos()*VALOR_KM);
    }

    // customs
    public void agregarKilometrosRecorridos(int kr) {
        if ( ( kr+getKilometrosRecorridos() ) >= getKilometrosAsignados() ) {
            setKilometrosRecorridos(getKilometrosAsignados());
            setEnFuncionamiento(false);
        } else {
            setKilometrosRecorridos(kr + getKilometrosRecorridos() );
        }
    
    }

    public String toString() {
        return super.toString()+
            "\nTipo Transporte  : Transporte Limitado\n"+
             "Gasto del mes    : "+calcularGastoMes()+"\n";
    }
    
}
