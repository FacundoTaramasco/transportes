
import paquetesPropios.Consola;

public class Main{

        public static void main(String[] args) {
        int opt = 0;
        do {
            Consola.MenuApp("\t\t\t*** SISTEMA DE TRANSPORTES ***\n\n"+
                           "1)  Agregar Transporte Limitado.\n"+
                           "2)  Agregar Transporte Libre Uso.\n"+
                           "3)  Editar Transportes.\n"+
                           "99) Salir.\n");
            opt = Consola.ingresoInt("Ingrese opcion : ");
            switch(opt) {
                case 1:
                    agregarTL();
                    break;
                case 2:
                    agregarTLU();
                    break;
                case 3:
                    editarTransportes();
                    break;
            }
        } while ( opt != 99 );
        System.out.println("Fin App!");
    }

    // *********************** ALTAS TRANSPORTES **************************** //

    public static void agregarTL() {
        Consola.limpiarConsola();
        System.out.println("Agregando Transporte Limitado ...");
        String patente = Consola.ingresoString("Ingrese patente : ");
        String conductor = Consola.ingresoString("Ingrese conductor : ");
        int ka  = Consola.ingresoInt("Ingrese kilometros Asignados : ");
        int ct = Consola.ingresoInt("Ingrese costo mantencion : ");
        Transporte tl = new TransporteLimitado(patente, conductor, ka, ct);
        if ( RegistroTransportes.agregarTransporte(tl) )
            System.out.println("Transporte Agregado.");
        else
            System.out.println("ERROR. Transporte Existente.");
        Consola.msjContinuar();
    }

    public static void agregarTLU() {
        Consola.limpiarConsola();
        System.out.println("Agregando Transporte Libre Uso ...");
        String patente = Consola.ingresoString("Ingrese patente : ");
        String conductor = Consola.ingresoString("Ingrese conductor : ");
        int ka  = Consola.ingresoInt("Ingrese kilometros Asignados : ");
        int ct = Consola.ingresoInt("Ingrese costo mantencion : ");
        Transporte tlu = new TransporteLibreUso(patente, conductor, ka, ct);
        if ( RegistroTransportes.agregarTransporte(tlu) )
            System.out.println("Transporte Agregado.");
        else
            System.out.println("ERROR. Transporte Existente.");
        Consola.msjContinuar();
    }



    // ******************* EDITAR TRANSPORTES ****************************** //

    public static void editarTransportes() {
        int aux = 0;
        do {
            Consola.MenuApp("\t\t\t *** Editando Transportes ***\n\n"+
                  "1)   Verificar Existencia Transporte.\n"+
                  "2)   Listado de Transportes.\n"+
                  "3)   Desplegar Factura de Transporte.\n"+
                  "4)   Agregar Kms. Recorridos de Transporte.\n"+
                  "5)   Reiniciar Kms. Recorridos de Transporte.\n"+
                  "99)  Salir. \n");
            aux = Consola.ingresoInt("Ingrese opcion : ");
            switch(aux) {
                case 1:
                    existenciaTransporte();
                    break;
                case 2:
                    listadoTransportes();
                    break;
                case 3:
                    mostrarFacturaTransporte();
                    break;
                case 4:
                    agregarKmRecorrido();
                    break;
                case 5:
                    reinicarKmRecorrido();
                    break;
            }

        }while(aux != 99);
    }

    public static void existenciaTransporte() {
        Consola.limpiarConsola();
        System.out.println("Verificar Existencia de Transporte.\n\n");
        String patente = Consola.ingresoString("Ingrese patente : ");
        if (RegistroTransportes.verificarTransporte(patente) ) {
            System.out.println("Transporte Existente.\n\n"+
                               RegistroTransportes.devolverTransporte(patente));
        } else {
            System.out.println("Transporte Inexiste");
        }
        Consola.msjContinuar();
    }

    public static void listadoTransportes() {
        Consola.limpiarConsola();
        System.out.println("Listado de Transportes : \n\n");
        RegistroTransportes.listaTransportes();
        Consola.msjContinuar();
    }

    public static void mostrarFacturaTransporte() {
        Consola.limpiarConsola();
        System.out.println("Mostrar Factura de Transporte.\n\n");
        String patente = Consola.ingresoString("Ingrese patente : ");
        System.out.println(RegistroTransportes.desplegarFactura(patente));
        Consola.msjContinuar();
    }

    public static void agregarKmRecorrido() {
        Consola.limpiarConsola();
        System.out.println("Agregando Kms. Recorridos.\n\n");
        String patente = Consola.ingresoString("Ingrese patente : ");
        int kmr = Consola.ingresoInt("Ingrese Kms. Recorridos : ");
        if ( RegistroTransportes.agregarKmsRecorridos(patente, kmr) ) {
            System.out.println("Kms. Recorridos agregados.\n" +
                               RegistroTransportes.devolverTransporte(patente) );
        } else {
            System.out.println("Transporte Inexiste");
        }
        Consola.msjContinuar();
    }

    public static void reinicarKmRecorrido() {
        Consola.limpiarConsola();
        System.out.println("Reiniciando Kms. Recorridos.\n\n");
        String patente = Consola.ingresoString("Ingrese patente : ");
        if ( RegistroTransportes.reiniciarKmsRecorridos(patente) ) {
            System.out.println("Kms. Recorridos reiniciados.\n" +
                               RegistroTransportes.devolverTransporte(patente) );
        } else {
            System.out.println("Transporte Inexiste");
        }
        Consola.msjContinuar();
    }
}
